-module(lcr).
-export([start/3]).

start(UID,Pred,Succ) ->
    Succ ! {UID},
    receive
	{PUID} ->
	    if
		PUID > UID ->
		    io:format("c'est pas moi~w", UID),
		    Succ ! {PUID};
		PUID == UID ->
		    io:format("C'est moi le boss~w", UID);
		PUID < UID ->
		    io:format("c'est ptet moi~w",UID),
		    Succ ! {UID};
		end

    end.

