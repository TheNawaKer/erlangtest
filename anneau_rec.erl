-module(anneau_rec).
-export([voisins/1,anneau/1,affiche/3,run/3]).

voisins(Uid) ->
    receive
	{pred,P} ->
	    receive
		{suiv,S} -> wait4it(Uid,P,S)
	    end
    end.

wait4it(Uid,P,S) ->
    receive
	{code,M,F,A} ->
	    apply(M,F,[Uid,P,S|A]),
	    wait4it(Uid,P,S)
    end.

ligne(1) ->
    Noeud = spawn(?MODULE,voisins,[random:uniform(1000000)]),
    {Noeud,Noeud,[Noeud]};
ligne(N) ->
    M = N div 2,
    {D1,F1,L1} = ligne(M),
    {D2,F2,L2} = ligne(N-M),
    F1 ! {suiv,D2},
    D2 ! {pred,F1},
    {D1,F2,L1++L2}.

anneau(N) ->
    {D,F,L} = ligne(N),
    D ! {pred,F},
    F ! {suiv,D},
    L.

affiche(Uid,P,S) -> io:format("~w <= ~w (~w) => ~w~n",[P,self(),Uid,S]).

run(_,_,[]) -> ok;
run(M,F,[H|T]) ->
    H ! {code,M,F,[]},
    run(M,F,T).
