-module(crible).
-export([start/1, calcul/0, generate/3, calcul/2]).

start(N) ->
	Pid = spawn(crible,calcul, []),
	generate(N,2, Pid).


generate(N,S,Pid) ->
	if S =< N ->
		Pid ! {self(), S},
		generate(N,S+1,Pid);
	true ->
		Pid ! {self(), stop}
	end.

calcul() ->
	receive
		{_,S} ->
			io:format("~p~n",[S]),
			Pid = spawn(crible, calcul, []),
			calcul(S,Pid)
	end.

calcul(Value,Pid) ->
	receive
		{_,S} -> 
			Pid ! {self(), S}
	end.

