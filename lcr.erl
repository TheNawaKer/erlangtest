-module(lcr).
-export([start/3]).

start(UID,Pred,Succ) ->
    io:format("Bonjour ~w~n", [UID]),
    Succ ! {UID},
    election(UID,Pred,Succ).

election(UID,Pred,Succ) ->
    receive
	{PUID} ->
	    if
		PUID > UID ->
		   % io:format("c'est pas moi ~w~n", [UID]),
		    Succ ! {PUID},
		    election(UID,Pred,Succ);
		PUID == UID ->
		    io:format("C'est moi le boss ~w~n", [UID]),
		    Succ ! {UID,"ok"};
		PUID < UID ->
		    %io:format("c'est ptet moi ~w~n",[UID]),
		    Succ ! {UID},
		    election(UID,Pred,Succ)
	    end;
	{BUID,_} -> 
	    io:format("le boss c'est ~w~n", [BUID]),
	    Succ ! {BUID,"ok"}

    end.

